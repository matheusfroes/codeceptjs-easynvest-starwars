exports.config = {
	tests: "./GET_test.js",
	output: "./output",
	helpers: {
		REST: {
			endpoint: "https://swapi.co/",
		}
	},
	include: {},
	bootstrap: null,
	mocha: {},
	name: "codeceptjs-easynvest-starwars",
	plugins: {}
};