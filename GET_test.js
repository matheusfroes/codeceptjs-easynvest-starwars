const should = require('chai').should();

const { I } = inject();

Feature('GET Star Wars');

Scenario('Verify first person', async () => {
	const res = await I.sendGetRequest('/api/people/1/');

	let status = res.status;
	status.should.be.eql(200);

	let name = res.data.name;
	name.should.be.eql('Luke Skywalker');

	let height = res.data.height;
	height.should.be.eql('172');

	let mass = res.data.mass;
	mass.should.be.eql('77');

	let hairColor = res.data.hair_color;
	hairColor.should.be.eql('blond');
	
	let skinColor = res.data.skin_color;
	skinColor.should.be.eql('fair');

	let eyeColor = res.data.eye_color;
	eyeColor.should.be.eql('blue');

	let birthYear = res.data.birth_year;
	birthYear.should.be.eql('19BBY');

	let gender = res.data.gender;
	gender.should.be.eql('male');
});

Scenario('Verify first planet', async () => {
	const res = await I.sendGetRequest('/api/planets/1/');

	let status = res.status;
	status.should.be.eql(200);

	let name = res.data.name;
	name.should.be.eql('Tatooine');

	let rotationPeriod = res.data.rotation_period;
	rotationPeriod.should.be.eql('23');

	let orbitalPeriod = res.data.orbital_period;
	orbitalPeriod.should.be.eql('304');

	let diameter = res.data.diameter;
	diameter.should.be.eql('10465');

	let climate = res.data.climate;
	climate.should.be.eql('arid');

	let gravity = res.data.gravity;
	gravity.should.be.eql('1 standard');

	let terrain = res.data.terrain;
	terrain.should.be.eql('desert');

	let surfaceWater = res.data.surface_water;
	surfaceWater.should.be.eql('1');

	let population = res.data.population;
	population.should.be.eql('200000');
});